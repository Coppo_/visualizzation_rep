#ifdef ENABLE_PHASESPACE
#include "libmotioncapture/phasespace.h"
#endif
#ifdef ENABLE_QUALISYS
#include <libmotioncapture/qualisys.h>
#endif
#ifdef ENABLE_VRPN
#include <libmotioncapture/vrpn.h>
#endif

// Object tracker
#include <libobjecttracker/object_tracker.h>
#include <libobjecttracker/cloudlog.hpp>

#include <fstream>
#include <future>
#include <mutex>
#include <wordexp.h> // tilde expansion

#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "test_marker");
  ros::NodeHandle nh;
  ros::Publisher pub = nh.advertise<PointCloud> ("pointCloud",1);
  
  libmotioncapture::MotionCapture* mocap = nullptr;//1492
  std::vector<libmotioncapture::Object> objects;

  PointCloud::Ptr msg (new PointCloud);
  msg->header.seq = 0;
  msg->header.frame_id = "base_link";
  msg->points.push_back(pcl::PointXYZ(-0.5, 1.0, 0.0));
  msg->points.push_back(pcl::PointXYZ(-0.5, 0.5, 0.0));
  msg->points.push_back(pcl::PointXYZ(-0.5, 0.0, 0.0));
  msg->points.push_back(pcl::PointXYZ(-0.5, -0.5, 0.0));
  msg->points.push_back(pcl::PointXYZ(-0.5, -1.0, 0.0));
  mocap = new libmotioncapture::MotionCaptureTest(
          0.01,
          objects,
          msg);
  
  pcl::PointCloud<pcl::PointXYZ>::Ptr markers(new pcl::PointCloud<pcl::PointXYZ>); //1565
          
  
  mocap->getPointCloud(markers); //1686
  msg->header.seq += 1;
  msg->points.resize(markers->size());
  for (size_t i = 0; i < markers->size(); ++i) {
  	const pcl::PointXYZ& point = markers->at(i);
  	msg->points[i].x = point.x;
  	msg->points[i].y = point.y;
  	msg->points[i].z = point.z;
  }
  
  ros::Rate loop_rate(4);
  
  while(nh.ok())
  {
  pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
  pub.publish(msg);
  ros::spinOnce ();
  loop_rate.sleep ();
  }
  
  return 0;
}
